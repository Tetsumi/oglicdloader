oglicdloader 
=============

Description:
-------------
Proof-of-concept code for loading an OpenGL ICD driver without WGL (opengl32.dll).
It's relying on the D3DKMT API introduced by Windows Vista.

The code has been tested on Windows Seven x64 with an AMD's gpu. The code works
fine without linking opengl32. The code is not 32 bits compliant.

To compile the test case with Mingw-w64:  
gcc -mwindows -std=c11 test.c oglicdloader.c -lgdi32


