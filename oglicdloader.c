#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include "oglicdloader.h"

typedef UINT D3DDDI_VIDEO_PRESENT_SOURCE_ID;
typedef UINT D3DKMT_HANDLE;

#if !defined(STATUS_SUCCESS)
#define STATUS_SUCCESS ((NTSTATUS)0x00000000)
#endif

typedef enum _KMTQUERYADAPTERINFOTYPE
{
	KMTQAITYPE_UMDRIVERPRIVATE         =  0,
	KMTQAITYPE_UMDRIVERNAME            =  1,
	KMTQAITYPE_UMOPENGLINFO            =  2,
	KMTQAITYPE_GETSEGMENTSIZE          =  3,
	KMTQAITYPE_ADAPTERGUID             =  4,
	KMTQAITYPE_FLIPQUEUEINFO           =  5,
	KMTQAITYPE_ADAPTERADDRESS          =  6,
	KMTQAITYPE_SETWORKINGSETINFO       =  7,
	KMTQAITYPE_ADAPTERREGISTRYINFO     =  8,
	KMTQAITYPE_CURRENTDISPLAYMODE      =  9,
	KMTQAITYPE_MODELIST                = 10,
	KMTQAITYPE_CHECKDRIVERUPDATESTATUS = 11,
} KMTQUERYADAPTERINFOTYPE;

typedef enum _KMTUMDVERSION
{
	KMTUMDVERSION_DX9 = 0,
	KMTUMDVERSION_DX10,
	KMTUMDVERSION_DX11,
} KMTUMDVERSION;

typedef struct _D3DKMT_OPENADAPTERFROMHDC
{
	HDC                             hDc;
	D3DKMT_HANDLE                   hAdapter;
	LUID                            AdapterLuid;
	D3DDDI_VIDEO_PRESENT_SOURCE_ID  VidPnSourceId;
} D3DKMT_OPENADAPTERFROMHDC;

typedef struct _D3DKMT_CLOSEADAPTER
{
	D3DKMT_HANDLE   hAdapter;
} D3DKMT_CLOSEADAPTER;


typedef struct _D3DKMT_QUERYADAPTERINFO
{
	D3DKMT_HANDLE           hAdapter;
	KMTQUERYADAPTERINFOTYPE Type;
	VOID*                   pPrivateDriverData;
	UINT                    PrivateDriverDataSize;
} D3DKMT_QUERYADAPTERINFO;

typedef struct _D3DKMT_OPENGLINFO
{
	WCHAR UmdOpenGlIcdFileName[MAX_PATH];
	ULONG Version;
	ULONG Flags;
} D3DKMT_OPENGLINFO;

typedef NTSTATUS (APIENTRY *PFND3DKMT_OPENADAPTERFROMHDC)(IN OUT D3DKMT_OPENADAPTERFROMHDC*);
typedef NTSTATUS (APIENTRY *PFND3DKMT_QUERYADAPTERINFO)(IN CONST D3DKMT_QUERYADAPTERINFO*);
typedef NTSTATUS (APIENTRY *PFND3DKMT_CLOSEADAPTER)(IN CONST D3DKMT_CLOSEADAPTER*);

typedef struct {
	HINSTANCE hInst;
	PFND3DKMT_OPENADAPTERFROMHDC OpenAdapterFromHdc;
	PFND3DKMT_QUERYADAPTERINFO QueryAdapterInfo;
	PFND3DKMT_CLOSEADAPTER CloseAdapter;
} D3DKMT_FUNCS;


static void ogl_driver_print (const OGL_DRIVER *ogld);
static bool ogl_driver_init (const HDC hdc, OGL_DRIVER *ogld);
static bool ogl_driver_final (OGL_DRIVER *ogld);

static bool d3dkmt_funcs_init (D3DKMT_FUNCS *d3df);
static bool d3dkmt_funcs_final (D3DKMT_FUNCS *d3df);
static bool d3dkmt_get_ogl_icd (const HDC hdc,
				const D3DKMT_FUNCS *d3df,
				UPATH *oglicdname);

typedef struct {
	int size;
	void* procs;
} PROC_TABLE;

void proctable_callback (PROC_TABLE *pt)
{
	assert(pt->size == 336);
}

bool ogl_create_context (const HDC hdc, OGL_CONTEXT *oglc)
{
	assert(hdc);
	assert(oglc);

	if (!ogl_driver_init(hdc, &oglc->ogld))
		return false;

	oglc->hdc = hdc;

	int number_of_pfd = oglc->ogld.DrvDescribePixelFormat(hdc,
							      0,
							      sizeof(PIXELFORMATDESCRIPTOR),
							      NULL);

	for (int i = 1; i < number_of_pfd; ++i) {
		oglc->ogld.DrvDescribePixelFormat(hdc,
						  i,
						  sizeof(PIXELFORMATDESCRIPTOR),
						  &oglc->pfd);

		if (oglc->pfd.dwFlags & PFD_DRAW_TO_WINDOW &&
		    oglc->pfd.dwFlags & PFD_SUPPORT_OPENGL &&
		    oglc->pfd.dwFlags & PFD_DOUBLEBUFFER &&
		    oglc->pfd.cColorBits == 32 &&
		    oglc->pfd.cRedBits == 8 &&
		    oglc->pfd.cGreenBits == 8 &&
		    oglc->pfd.cBlueBits == 8 &&
		    oglc->pfd.cAlphaBits == 8 &&
		    oglc->pfd.cDepthBits == 24 &&
		    oglc->pfd.cStencilBits == 8) {
			oglc->ogld.DrvSetPixelFormat(hdc, i);
			break;
		}
		    
	}

	HGLRC hglrc = oglc->ogld.DrvCreateContext(hdc);

	if (!hglrc) {
		ogl_driver_final(&oglc->ogld);
		return false;
	}

	oglc->hglrc = hglrc;

	PROC_TABLE *procs_table = oglc->ogld.DrvSetContext(hdc,
							   hglrc,
							   proctable_callback);

	if (!procs_table || procs_table->size != 336) {
		oglc->ogld.DrvDeleteContext(hglrc);
		ogl_driver_final(&oglc->ogld);
		return false;
	}

	memcpy(&oglc->glNewList, &procs_table->procs, sizeof(void*)*336);

	return true;
}

bool ogl_delete_context(OGL_CONTEXT *oglc)
{
	assert(oglc);
	assert(oglc->ogld.hInst);
	
	oglc->ogld.DrvReleaseContext(oglc->hglrc);
	oglc->ogld.DrvSetContext(oglc->hdc, NULL, NULL);
	oglc->ogld.DrvDeleteContext(oglc->hglrc);
	ogl_driver_final(&oglc->ogld);
	
	*oglc = (OGL_CONTEXT){0};
}

static void ogl_driver_print (const OGL_DRIVER *ogld)
{
	assert(ogld);
	puts("OGL_DRIVER:");
	printf("   Name: %S\n", ogld->name);
	printf("   hInst: %llu\n", ogld->hInst);
	printf("   DrvCreateContext: %p\n", ogld->DrvCreateContext);
	printf("   DrvDeleteContext: %p\n", ogld->DrvDeleteContext);
	printf("   DrvSetContext: %p\n", ogld->DrvSetContext);
	printf("   DrvReleaseContext: %p\n", ogld->DrvReleaseContext);
	printf("   DrvCopyContext: %p\n", ogld->DrvCopyContext);
	printf("   DrvCreateLayerContext: %p\n", ogld->DrvCreateLayerContext);
	printf("   DrvShareLists: %p\n", ogld->DrvShareLists);
	printf("   DrvGetProcAddress: %p\n", ogld->DrvGetProcAddress);
	printf("   DrvDescribeLayerPlane: %p\n", ogld->DrvDescribeLayerPlane);
	printf("   DrvSetLayerPaletteEntries: %p\n",
	       ogld->DrvSetLayerPaletteEntries);
	printf("   DrvGetLayerPaletteEntries: %p\n",
	       ogld->DrvGetLayerPaletteEntries);
	printf("   DrvRealizeLayerPalette: %p\n", ogld->DrvRealizeLayerPalette);
	printf("   DrvSwapLayerBuffers: %p\n", ogld->DrvSwapLayerBuffers);
	printf("   DrvDescribePixelFormat: %p\n", ogld->DrvDescribePixelFormat);
	printf("   DrvSetPixelFormat: %p\n", ogld->DrvSetPixelFormat);
	printf("   DrvSwapBuffers: %p\n", ogld->DrvSwapBuffers);
}

static bool ogl_driver_init (const HDC hdc, OGL_DRIVER *ogld)
{
	assert(ogld);
	assert(!ogld->hInst);
	
	D3DKMT_FUNCS df;
	
	if (!d3dkmt_funcs_init(&df))
		return false;

	if (!hdc || !d3dkmt_get_ogl_icd(hdc, &df, &ogld->name)) {
		// TODO:
		// rely on EnumDisplayDevices to create a DC on the
		// primary display device.
	}

	if (wcslen(ogld->name) == 0)
		return false;

	HINSTANCE hInst = LoadLibrary(ogld->name);

	if (!hInst)
		return false;

	ogld->hInst = hInst;
	ogld->DrvCreateContext =
		(void *)GetProcAddress(hInst, "DrvCreateContext");
	ogld->DrvDeleteContext =
		(void *)GetProcAddress(hInst, "DrvDeleteContext");
	ogld->DrvSetContext =
		(void *)GetProcAddress(hInst, "DrvSetContext");
	ogld->DrvReleaseContext =
		(void *)GetProcAddress(hInst, "DrvReleaseContext");
	ogld->DrvCopyContext =
		(void *)GetProcAddress(hInst, "DrvCopyContext");
	ogld->DrvCreateLayerContext =
		(void *)GetProcAddress(hInst, "DrvCreateLayerContext");	
	ogld->DrvShareLists =
		(void *)GetProcAddress(hInst, "DrvShareLists");
	ogld->DrvGetProcAddress =
		(void *)GetProcAddress(hInst, "DrvGetProcAddress");
	ogld->DrvDescribeLayerPlane =
		(void *)GetProcAddress(hInst, "DrvDescribeLayerPlane");
	ogld->DrvSetLayerPaletteEntries =
		(void *)GetProcAddress(hInst, "DrvSetLayerPaletteEntries");
	ogld->DrvGetLayerPaletteEntries =
		(void *)GetProcAddress(hInst, "DrvGetLayerPaletteEntries");
	ogld->DrvRealizeLayerPalette =
		(void *)GetProcAddress(hInst, "DrvRealizeLayerPalette");
	ogld->DrvSwapLayerBuffers =
		(void *)GetProcAddress(hInst, "DrvSwapLayerBuffers");
	ogld->DrvDescribePixelFormat =
		(void *)GetProcAddress(hInst, "DrvDescribePixelFormat");
	ogld->DrvSetPixelFormat =
		(void *)GetProcAddress(hInst, "DrvSetPixelFormat");
	ogld->DrvSwapBuffers =
		(void *)GetProcAddress(hInst, "DrvSwapBuffers");

	d3dkmt_funcs_final(&df);
	return true;
}

static bool ogl_driver_final (OGL_DRIVER *ogld)
{
	assert(ogld);
	assert(ogld->hInst);

	FreeLibrary(ogld->hInst);

	*ogld = (OGL_DRIVER){
		.name = {'0'},
		.hInst = NULL,
		.DrvCreateContext = NULL,
		.DrvDeleteContext = NULL,
		.DrvSetContext = NULL,
		.DrvReleaseContext = NULL,
		.DrvCopyContext = NULL,
		.DrvCreateLayerContext = NULL,
		.DrvShareLists = NULL,
		.DrvGetProcAddress = NULL,
		.DrvDescribeLayerPlane = NULL,
		.DrvSetLayerPaletteEntries = NULL,
		.DrvGetLayerPaletteEntries = NULL,
		.DrvRealizeLayerPalette = NULL,
		.DrvSwapLayerBuffers = NULL,
		.DrvDescribePixelFormat = NULL,
		.DrvSetPixelFormat = NULL,
		.DrvSwapBuffers = NULL
	};
}

static bool d3dkmt_funcs_init (D3DKMT_FUNCS *d3df)
{
	assert(d3df);
	
	HINSTANCE hInst = LoadLibrary(L"gdi32.dll");

	if (!hInst)
		return false;
	
	PFND3DKMT_OPENADAPTERFROMHDC pfnKTOpenAdapterFromHdc =
		(PFND3DKMT_OPENADAPTERFROMHDC)
		GetProcAddress(hInst, "D3DKMTOpenAdapterFromHdc");

	PFND3DKMT_QUERYADAPTERINFO pfnKTQueryAdapterInfo =
		(PFND3DKMT_QUERYADAPTERINFO)
		GetProcAddress(hInst, "D3DKMTQueryAdapterInfo");

	PFND3DKMT_CLOSEADAPTER pfnKTCloseAdapter =
		(PFND3DKMT_CLOSEADAPTER)
		GetProcAddress(hInst, "D3DKMTCloseAdapter");

	if (!pfnKTOpenAdapterFromHdc ||
	    !pfnKTQueryAdapterInfo ||
	    !pfnKTCloseAdapter) {
		FreeLibrary(hInst);
		return FALSE;
	}

	*d3df = (D3DKMT_FUNCS){
		.hInst = hInst,
		.OpenAdapterFromHdc = pfnKTOpenAdapterFromHdc,
		.QueryAdapterInfo = pfnKTQueryAdapterInfo,
		.CloseAdapter = pfnKTCloseAdapter
	};

	return true;
}

static bool d3dkmt_funcs_final (D3DKMT_FUNCS *d3df)
{
	assert(d3df);
	assert(d3df->hInst);
	
	FreeLibrary(d3df->hInst);
	
	*d3df = (D3DKMT_FUNCS){
		.hInst = NULL,
		.OpenAdapterFromHdc = NULL,
		.QueryAdapterInfo = NULL,
		.CloseAdapter = NULL
	};

	return true;
}

static bool d3dkmt_get_ogl_icd (const HDC hdc,
				const D3DKMT_FUNCS *d3df,
				UPATH *oglicdname)
{
	assert(hdc);
	assert(d3df);
	assert(oglicdname);
	
	D3DKMT_OPENADAPTERFROMHDC oafh = {.hDc = hdc};

	if (d3df->OpenAdapterFromHdc(&oafh) != STATUS_SUCCESS)
		return false;
	
	D3DKMT_OPENGLINFO oglInfo;
	
	D3DKMT_QUERYADAPTERINFO QueryAdapterInfo = {
		.hAdapter = oafh.hAdapter,
		.Type = KMTQAITYPE_UMOPENGLINFO,
		.pPrivateDriverData = &oglInfo,
		.PrivateDriverDataSize = sizeof(oglInfo)
	};
	
	if (d3df->QueryAdapterInfo(&QueryAdapterInfo) != STATUS_SUCCESS)
		return false;

	D3DKMT_CLOSEADAPTER ca = {.hAdapter = oafh.hAdapter};
	
	d3df->CloseAdapter(&ca);
	wcscpy((WCHAR*)oglicdname, oglInfo.UmdOpenGlIcdFileName);
	
	return true;
}
