#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "oglicdloader.h"

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_DESTROY) {
		PostQuitMessage(WM_QUIT);
		return 0;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}

int WINAPI WinMain (HINSTANCE hInstance,
		    HINSTANCE hPrevInstance,
		    PSTR szCmdLine,
		    int iCmdShow)
{
	WNDCLASS wc      = {0}; 
	wc.lpfnWndProc   = WndProc;
	wc.hInstance     = hInstance;
	wc.hbrBackground = (HBRUSH)(COLOR_BACKGROUND);
	wc.lpszClassName = L"oglicdloader test";
	wc.style = CS_OWNDC;

	if (!RegisterClass(&wc))
		return EXIT_FAILURE;

	HWND hWnd = CreateWindow(wc.lpszClassName,
				 L"oglicdloader test",
				 WS_OVERLAPPEDWINDOW|WS_VISIBLE,
				 0,0,640,480,0,0,hInstance,0);

	if (!hWnd)
		return EXIT_FAILURE;

	HDC hdc = GetDC(hWnd);
	OGL_CONTEXT oglc = {0};
	
	if (!ogl_create_context(hdc, &oglc))
		return EXIT_FAILURE;
	
	oglc.glClearColor(0.5f, 0.0f, 0.0f, 0.0f);

	MSG msg = {0};

	while(GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg); 
		DispatchMessage(&msg);		
		oglc.glClear(GL_COLOR_BUFFER_BIT);
		oglc.glBegin(GL_TRIANGLES);
		oglc.glVertex3f( 1.0f, -1.0f, 0.0f);
		oglc.glVertex3f( 0.0f,  1.0f, 0.0f);
		oglc.glVertex3f(-1.0f, -1.0f, 0.0f);
		oglc.glEnd();
		oglc.ogld.DrvSwapBuffers(oglc.hdc);
	}
	
	ogl_delete_context(&oglc);
}
